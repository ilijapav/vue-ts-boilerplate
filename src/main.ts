import registerBaseComponents from '@/shared/components/components';
import Vue from 'vue';
import App from './App.vue';
import router from './router';
import vuetify from './shared/plugins/vuetify';
import store from './shared/store';

Vue.config.productionTip = false;

registerBaseComponents();

new Vue({
  router,
  store,
  vuetify,
  render: (h) => h(App),
}).$mount('#app');
