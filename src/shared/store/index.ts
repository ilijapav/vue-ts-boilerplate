import UserModule from '@/shared/store/modules/user.store-module';
import Vue from 'vue';
import Vuex from 'vuex';

Vue.use(Vuex);

const store = new Vuex.Store({
  modules: {
    User: UserModule,
  },
});
export default store;
