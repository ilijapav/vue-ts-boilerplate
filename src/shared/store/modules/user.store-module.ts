import api from '@/shared/api/api';
import { Action, Module, Mutation, VuexModule } from 'vuex-module-decorators';
import { User } from '../../models/User';

@Module({ namespaced: true })
class UserModule extends VuexModule {
  user: User = null;
  accessToken: string = null;

  @Mutation
  private setUser(value: User): void {
    this.user = value;
  }

  @Mutation
  private setAccessToken(value: string): void {
    this.accessToken = value;
  }

  @Action({ rawError: true })
  public updateUser(user: User): void {
    this.context.commit('setUser', user);
  }

  @Action({ rawError: true })
  public updateAccessToken(value: string): void {
    this.context.commit('setAccessToken', value);
  }

  @Action
  public async loginUser(email: string, password: string) {
    try {
      const res = await api.auth.login(email, password);
      this.updateUser(res.data.user);
      console.log('prosao');
      this.updateAccessToken(res.data.accessToken);
    } catch (error) {
      console.error(error);
    }
  }
}
export default UserModule;
