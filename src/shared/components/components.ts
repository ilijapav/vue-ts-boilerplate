import Vue from 'vue';
import AppButton from './AppButton.vue';
import AppInput from './AppInput.vue';

export default () => {
  Vue.component('app-input', AppInput);
  Vue.component('app-button', AppButton);
};
