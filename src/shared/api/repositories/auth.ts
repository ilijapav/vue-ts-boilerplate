import httpClient from '../axios.config';
import { ApiResponse } from './../../models/ApiResponse';
import { User } from './../../models/User';

const END_POINT = '/auth';

export default {
  async login(email: string, password: string): Promise<ApiResponse<{ user: User; accessToken: string }>> {
    await httpClient.get('/posts/1');
    return {
      data: { user: { email, password }, accessToken: 'OVO_JE_ACCESS_TOKEN' },
      message: 'success',
    };
  },
};
