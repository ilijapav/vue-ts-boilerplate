import authRepository from './repositories/auth';

export default {
  auth: authRepository,
};
