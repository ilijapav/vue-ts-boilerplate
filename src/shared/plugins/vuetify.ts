import Vue from 'vue';
import Vuetify from 'vuetify/lib/framework';

Vue.use(Vuetify);

export default new Vuetify({
  theme: {
    themes: {
      light: {
        primary: '#4caf50',
        secondary: '#673ab7',
        accent: '#9c27b0',
        error: '#f44336',
        warning: '#ffc107',
        info: '#2196f3',
        success: '#8bc34a',
      },
    },
    options: { customProperties: true },
  },
});
