import Vue from 'vue';
import VueRouter, { RouteConfig } from 'vue-router';
import Login from './auth/Login.vue';
import Home from './home/Home.vue';
import store from './shared/store';

const authGuard = (to: any, from: any, next: any) => {
  const accessToken: string = store.state.User.accessToken;
  console.log(accessToken);
  next();
};

Vue.use(VueRouter);

const routes: Array<RouteConfig> = [
  {
    path: '/',
    name: 'Home',
    component: Home,
  },
  {
    path: '/login',
    name: 'Login',
    component: Login,
    beforeEnter: authGuard,
  },
  {
    path: '/about',
    name: 'About',
    // route level code-splitting
    // this generates a separate chunk (about.[hash].js) for this route
    // which is lazy-loaded when the route is visited.
    component: () => import(/* webpackChunkName: "about" */ './about/About.vue'),
  },
];

const router = new VueRouter({
  mode: 'history',
  base: process.env.BASE_URL,
  routes,
});

export default router;
